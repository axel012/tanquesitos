import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

export interface CanComponentDeActivate{
     beforeunload(): Observable<boolean>|Promise<boolean>|boolean;
}

@Injectable()
export class InactivateGuard implements CanDeactivate<CanComponentDeActivate> {
  canDeactivate(
    component: CanComponentDeActivate, 
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return component.beforeunload();
  }
}
