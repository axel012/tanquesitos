import { TestBed, async, inject } from '@angular/core/testing';

import { InactivateGuard } from './inactivate.guard';

describe('InactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InactivateGuard]
    });
  });

  it('should ...', inject([InactivateGuard], (guard: InactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
