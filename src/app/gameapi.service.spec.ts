import { TestBed, inject } from '@angular/core/testing';

import { GameapiService } from './gameapi.service';

describe('GameapiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameapiService]
    });
  });

  it('should be created', inject([GameapiService], (service: GameapiService) => {
    expect(service).toBeTruthy();
  }));
});
