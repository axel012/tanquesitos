import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";
import { Router } from "@angular/router";
import { map, catchError } from "rxjs/operators";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  host: {
    "(document:keyup)": "keyEvent($event)"
  }
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  userNotFound: Boolean = false;
  dologin: Boolean = false;
  messageError: String;

  constructor(private authservice: AuthService, private router: Router) {
    this.authservice.loggedIn.subscribe(loggedIn => {
      if (loggedIn) this.router.navigate(["/menu"]);
      else {
        if (this.dologin) {
          this.userNotFound = true;
          this.messageError = "404\nUser Not Found";
          setTimeout(() => {
            this.userNotFound = false;
            this.router.navigate(["/register"]);
          }, 3000);
        }
      }
    });
  }

  doLogin() {
    this.authservice.doLogin(this.username, this.password);
    this.dologin = true;
  }

  ngOnInit() {
    this.userNotFound = false;
  }

  //replace special characters input with ""
  tidyUserInput(event) {
    try {
      this.username = this.username.replace(/[^ña-z^0-9^ÑA-Z]/g, "");
      this.username = this.username.substring(0, 10);
    } catch (e) {}
  }
  //switch between login->register
  changeChannel() {
    this.router.navigate(["register"]);
  }

  keyEvent(keyevent) {
    if (keyevent.key === "ArrowLeft" || keyevent.key === "ArrowRight") {
      this.changeChannel();
    }
  }
}
