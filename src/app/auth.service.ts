import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { GameapiService } from "./gameapi.service";

declare var Howler: any;
declare var Stage: any;
declare var TankMap: any;

@Injectable()
export class AuthService {
  loggedIn: Subject<boolean>;
  private _isLoggedIn: boolean;

  setLogedIn(val) {
    this._isLoggedIn = val;
  }

  get isLoggedIn() {
    return this._isLoggedIn;
  }

  constructor(private gameAPI: GameapiService) {
    this.loggedIn = new Subject();
    this.getLoggedIn();
    this.loggedIn.subscribe(logged => {
      this.setLogedIn(logged);
    });
  }
  /**
   * Function: doLogin(username,password)
   * Description: auth login
   * Params:  username - registered user username
   *          password - registered password for that username
   * Return Value(s): none
   * Observable: onSuccess - set loggedIn = true
   *             onError   - set loggedIn = false
   */

  doLogin(username: string, password: string) {
    this.gameAPI.login(username, password).subscribe(
      resp => {
        this.loggedIn.next(true);
      },
      err => {
        this.loggedIn.next(false);
      }
    );
  }
  /**
   * Function: getLoggedIn()
   * Description:  Check if user is logged in
   * Params: none
   * Return Value(s): none
   * Observable: onSuccess - set loggedIn = @true  - user already logged in
   *                                        @false - user not logged in
   *             onError   - set errorFlag
   */
  getLoggedIn() {
    this.gameAPI.getLoggedIn().subscribe(
      (resp: any) => {
        this.loggedIn.next(resp.loggedIn);
      },
      err => {
        this.loggedIn.next(false);
      }
    );
  }

  /**
   * Function: logout()
   * Description: destroy session for that user
   * Params:none
   * Return Value(s): none
   * Observable: onSuccess - set loggedIn = false
   *             onError   - set error flag
   */
  logout() {
    this.gameAPI.logout().subscribe(
      resp => {
        //unload game libraries
        Howler.unload();
        Stage.Destroy();
        TankMap.Destroy();
        this.loggedIn.next(false);
      },
      err => {}
    );
  }
}
