import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../environments/environment';

@Injectable()
export class GameapiService {
  
  constructor(private http:HttpClient) { }
 
  private APIEndPoint = {
    "LOGIN": "/login",
    "REGISTER": "/signup",
    "LOGOUT": "/logout",
    "GAME": "/game"
  };

/**
 * Function: login(username,password)
 * Description: api login
 * Params:  username:string - registered user username
 *          password:string - registered password for that username
 * Return Value(s): Observable
 *                 
 */
  login(username:string, password:string){
    return this.http.post(environment.apiUrl + this.APIEndPoint.LOGIN,{
      username:username,
      password:password,
    },{withCredentials:true});
  }

/**
 * Function: getLoggedIn()
 * Description: Check if user is logged in
 * Params:none
 * Return Value(s): Observable
 *                  
 */
  getLoggedIn(){
    return this.http.get(environment.apiUrl + this.APIEndPoint.LOGIN,{withCredentials:true});
  }

 
  /**
 * Function: register(username,password)
 * Description: register new user
 * Params:  username:string - username to register
 *          password:string - password for that username
 * Return Value(s): Observable
 *                  
 */
register(username:string,password:string,name:string){
  return this.http.post(environment.apiUrl + this.APIEndPoint.REGISTER,{
    username: username,
    password: password,
    name:name
  },{withCredentials:true});
}

/**
 * Function: logout()
 * Description: destroy session for that user
 * Params:none
 * Return Value(s): Observable
 *                  
 */
logout(){
  return this.http.get(environment.apiUrl + this.APIEndPoint.LOGOUT,{withCredentials:true});
}

/**
 * Function: getRooms(start,count)
 * Description: Obtain a list of rooms or empty array if none
 * Params:	start:Number - number of rooms to skip to start searching
			count:Number - number of rooms returned in search
 * Return Value(s): Observable
 *                  
 */
getRooms(start,count){
  return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + `/room/create/${start}/${count}`,{withCredentials:true});
}

/**
 * Function: createRoom()
 * Description: creates a room for playing
 * Params:none
 * Return Value(s): Observable
 *                  
 */
createRoom(){
  return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + "/room/create",{withCredentials:true});
}

/**
 * Function: joinRoom(id)
 * Description: join a room by id
 * Params:	id:Number - Target room id to join
 * Return Value(s): Observable
 *                  
 */
joinRoom(id){
  return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + `/room/join/${id}`,{withCredentials:true});
}

/**
 * Function: getInRoom()
 * Description: check if the user is in a room, returns status 200 or 404
 * Params:none
 * Return Value(s): Observable
 *                  
 */
getInRoom(){
  return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + "/room/active",{withCredentials:true});
}


/**
 * Function: getInRoomPlaying()
 * Description: check if the user is in a room and currently playing, returns status 200 or 403
 * Params:none
 * Return Value(s): Observable
 *                  
 */
getInRoomPlaying(){
    return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + "/room/active/playing",{withCredentials:true});
}


/**
 * Function: getInRoomNotPlaying()
 * Description: set a user as inactive, returns status 200 
 * Params:none
 * Return Value(s): Observable
 *                  
 */
getInRoomNotPlaying(){
    return this.http.get(environment.apiUrl + this.APIEndPoint.GAME + "/room/active/disconnect",{withCredentials:true});
}

/**
 * Function: getScoreboard(filter)
 * Description: get the scoreboard of players with a given filter
 * Params:filter:String - Availabe filters {"wins","loses","kills","deaths","blocks"}
 * Return Value(s): Observable
 *                  
 */
getScoreboard(filter){
  return this.http.get(environment.apiUrl + `/scoreboard/${filter}`,{withCredentials:true});
}

}
