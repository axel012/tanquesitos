import { Component, OnInit } from "@angular/core";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";


declare var gameLoader: any;
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";
  loggedIn: boolean;
  constructor(private authservice: AuthService, private router: Router) {
	  
    this.authservice.loggedIn.subscribe(logged => {
      this.loggedIn = logged;
    });
	//load game libs
    let gl = new gameLoader();
    gl.load();
  }
  
  logout() {
    this.authservice.logout();
    this.router.navigate(["/login"]);
  }

  ngOnInit() {}
}
