import { Component, OnInit } from '@angular/core';
import { Router } from '../../../node_modules/@angular/router';
import { GameapiService } from '../gameapi.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {
 
  currentChannel:any;
  channelIndex:any;
  channels = ["#10 Winners","#10 Lossers","#10 Killers","#10 Zombies","#10 Breakers"];
  filters = ["wins","loses","kills","deaths","blocks"];
  data:any;
  constructor(private router:Router,private gameAPI:GameapiService) { 
    this.channelIndex = -1;
    this.changeChannel(1);
  }

  ngOnInit() {
  }
  
  backToMenu(){
    this.router.navigate(["/menu"]);
  }

 
  changeChannel(dir){
    this.channelIndex+=dir;
	if(this.channelIndex < 0)
		this.channelIndex = this.filters.length - 1;
    this.channelIndex %= this.channels.length;
    this.currentChannel = this.channels[this.channelIndex];
    this.gameAPI.getScoreboard(this.filters[this.channelIndex]).subscribe((res)=>{
      this.data = res;
    },(err)=>{});
  }
}
