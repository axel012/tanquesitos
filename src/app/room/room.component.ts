import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { GameapiService } from '../gameapi.service';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  @Input() room;
  @Output() joinProblem: EventEmitter<any> = new EventEmitter();
  constructor(private gameapi:GameapiService,private router:Router) { }

  ngOnInit() {
  }

  join(){
    this.gameapi.joinRoom(this.room.id).subscribe((res)=>{
      this.router.navigate(["/game"]);
    },(err)=>{
        this.joinProblem.emit({msg:"user already playing in another browser instance"});
    });
  }

}
