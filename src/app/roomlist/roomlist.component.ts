import { Component, OnInit } from "@angular/core";
import { GameapiService } from "../gameapi.service";
import { Router } from "../../../node_modules/@angular/router";

@Component({
  selector: "app-roomlist",
  templateUrl: "./roomlist.component.html",
  styleUrls: ["./roomlist.component.css"]
})
export class RoomlistComponent implements OnInit {
  error: any;
  rooms: any;
  loadedRooms: any;
  maxLoadCount: any;
  shownRoomCount: any;
  shownRoomIndex: any;
  isLoading: any;
  lastLoadSize: any;
  noMoreItems: any;
  msgProblem: Boolean;
  messageError: String;

  constructor(private gameAPI: GameapiService, private router: Router) {
    this.shownRoomIndex = -1;
    this.shownRoomCount = 5;
    this.maxLoadCount = 6;
    this.loadedRooms = [];
    this.noMoreItems = false;
    this.msgProblem = false;
  }

  ngOnInit() {
    this.loadRooms();
    this.isLoading = true;
  }
  onJoinProblem(event) {
    this.msgProblem = true;
    this.messageError = event.msg;
    setTimeout(() => {
      this.msgProblem = false;
    }, 3000);
	//reload rooms
	this.loadRooms();
  }

  backToMenu() {
    this.router.navigate(["/menu"]);
  }

  prevRooms() {
    if (this.shownRoomIndex <= 0) return;
    this.shownRoomIndex--;
    this.rooms = this.loadedRooms.slice(
      this.shownRoomIndex,
      this.shownRoomIndex + this.shownRoomCount
    );
  }

  nextRooms() {
    if (
      this.shownRoomIndex +
        Math.min(this.shownRoomCount, this.loadedRooms.length) >=
      this.loadedRooms.length
    ) {
      if (this.noMoreItems) return;
      this.isLoading = true;
      this.lastLoadSize = this.loadedRooms.length;
      this.loadMore(
        this.shownRoomIndex + this.shownRoomCount,
        this.maxLoadCount
      );
      return;
    }
    this.shownRoomIndex++;
    this.rooms = this.loadedRooms.slice(
      this.shownRoomIndex,
      this.shownRoomIndex + this.shownRoomCount
    );
	//remove duplicates
	this.rooms =  this.rooms.filter((val, index, self) =>
		index === self.findIndex((t) => (
		t.id === val.id
	)
	));
	
  }

  loadMore(start, count) {
    this.gameAPI.getRooms(start, count).subscribe(
      res => {
        this.loadedRooms = this.loadedRooms.concat(res["rooms"]);
        setTimeout(() => {
          this.isLoading = false;
          if (this.lastLoadSize === this.loadedRooms.length) {
            this.noMoreItems = true;
          }
          this.nextRooms();
        }, 1000);
      },
      err => {
        this.error = err;
      }
    );
  }

  loadRooms() {
    this.rooms = [];
    this.gameAPI.getRooms(0, this.maxLoadCount).subscribe(
      res => {
        this.loadedRooms = res["rooms"];
        this.nextRooms();
        this.isLoading = false;
      },
      err => {
        this.error = err;
      }
    );
  }
}
