import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import { AuthService } from './auth.service';
import { isBoolean } from 'util';

import {map, catchError} from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate,CanActivateChild {

   constructor(private authservice:AuthService,private router:Router){
   
    }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean  {
    let url: string = state.url;
    if(this.authservice.isLoggedIn) return true;
    return this.authservice.loggedIn.pipe(map(e=>{
      if(e){
      return true;
      }else{
        this.router.navigate(["/login"]);
      }
    },(err)=>{}
     ));    
}

canActivateChild(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot): Observable<boolean> | boolean {
  return this.canActivate(route, state);
}


}
  

