import { Component, OnInit } from "@angular/core";
import { Router } from "../../../node_modules/@angular/router";
import { GameapiService } from "../gameapi.service";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"],
  host: {
    "(document:keyup)": "keyEvent($event)"
  }
})
export class RegisterComponent implements OnInit {
  password: string;
  username: string;
  name: string;
  message:string;
  isShowingMessage:Boolean = false;
  constructor(private router: Router,private gameapi:GameapiService,private authservice:AuthService) {

  }

  ngOnInit() {}

  doRegister() {
    this.gameapi.register(this.username,this.password,this.name).subscribe((res)=>{
      this.isShowingMessage = true
      this.message = `<font color="#00ff68">Account created</font>`;
      setTimeout(()=>{
        this.isShowingMessage = false;
        this.authservice.doLogin(this.username,this.password);
      },1500);
      
    },(err)=>{
      var field = err.error.error.errmsg.split(".$")[1].split("dup key")[0];
          field = field.substring(0,field.lastIndexOf("_"));
          this.isShowingMessage = true
            this.message = `<font color="#00ff68">Woops\n picked </font><b>${field}</b>`;
            setTimeout(() => {
              this.isShowingMessage = false;
            }, 1500);
    })
  }

  //switch between register->login
  changeChannel() {
    this.router.navigate(["login"]);
  }
  
  keyEvent(keyevent) {
    if (keyevent.key === "ArrowLeft" || keyevent.key === "ArrowRight") {
      this.changeChannel();
    }
  }

//replace special characters input with ""
  tidyUserInput(){
    try{
    this.name = this.name.replace(/[^ña-z^0-9^ÑA-Z]/g,"");
    this.name = this.name.substring(0,10);
    }catch(e){}
  }
  
    tidyUserInputUsername(){
    try{
    this.username = this.username.replace(/[^ña-z^0-9^ÑA-Z]/g,"");
    this.username = this.username.substring(0,10);
    }catch(e){}
  }
}
