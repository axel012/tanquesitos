import { Component, OnInit } from "@angular/core";
import { GameapiService } from "../gameapi.service";
import { Router } from "../../../node_modules/@angular/router";
import { Subject } from "rxjs/Subject";

declare var Howler: any;
@Component({
  selector: "app-gamemenu",
  templateUrl: "./gamemenu.component.html",
  styleUrls: ["./gamemenu.component.css"],
  host: {
    "(document:keyup)": "keyEvent($event)"
  }
})
export class GamemenuComponent implements OnInit {
  currentIndex: any;
  numberOfIndices: any;
  inRoom: Subject<Boolean>;
  msgProblem: Boolean;
  messageError: String;

  constructor(private gameAPI: GameapiService, private router: Router) {
    this.inRoom = new Subject<Boolean>();
    this.inRoom.next(false);
    this.msgProblem = false;

    //Subscribe to check if user is in room or not
    gameAPI.getInRoom().subscribe(
      success => {
        if (success) {
          this.inRoom.next(true);
        } else {
          this.inRoom.next(false);
        }
      },
      err => {
        this.inRoom.next(false);
      }
    );
  }

  ngOnInit() {
    this.currentIndex = 0;
    this.numberOfIndices = 3;
  }

  nextIndex() {
    this.currentIndex++;
    this.currentIndex %= this.numberOfIndices;
  }

  prevIndex() {
    this.currentIndex--;
    if (this.currentIndex < 0) {
      this.currentIndex = this.numberOfIndices - 1;
    }
  }

  keyEvent(event) {
    if (event.key === "ArrowUp") this.prevIndex();
    if (event.key === "ArrowDown") this.nextIndex();
    if (event.key === "Enter") {
      switch (this.currentIndex) {
        case 0:
          this.createRoom();
          break;
        case 1:
          this.router.navigate(["/menu/rooms"]);
          break;
        case 2:
          this.router.navigate(["/menu/scoreboard"]);
          break;
      }
    }
  }

  createRoom() {
    this.gameAPI.createRoom().subscribe(
      res => {
        this.router.navigate(["/game"]);
      },
      err => {
        this.msgProblem = true;
        this.messageError = err.error;
        setTimeout(() => {
          this.msgProblem = false;
          if (err.error === "Permission denied") {
            Howler.unload();
            this.router.navigate(["/login"]);
          }
        }, 3000);
      }
    );
  }
}
