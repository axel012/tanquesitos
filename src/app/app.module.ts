import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {GamemenuComponent} from './gamemenu/gamemenu.component';
import {GameinstanceComponent} from './gameinstance/gameinstance.component';
import {RoomlistComponent} from './roomlist/roomlist.component';
import {RoomComponent} from './room/room.component';
import {ScoreboardComponent} from './scoreboard/scoreboard.component';
import {RouterModule} from '@angular/router';
import {AuthGuard} from './auth.guard';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';


import {AuthService} from './auth.service';
import {GameapiService} from './gameapi.service';
import {ReconnectGuard} from './reconnect.guard';
import {PlayGuard} from './play.guard';
import {InactivateGuard} from './inactivate.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    GamemenuComponent,
    GameinstanceComponent,
    RoomlistComponent,
    RoomComponent,
    ScoreboardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'game', component: GameinstanceComponent, canActivate: [AuthGuard,PlayGuard],canDeactivate:[InactivateGuard]},
      {
        path: 'menu', canActivate: [AuthGuard],
        children: [
          {path: '', component: GamemenuComponent},
          {path: 'rooms', component: RoomlistComponent, canActivateChild: [AuthGuard], canActivate: [ReconnectGuard]},
          {
            path: 'scoreboard',
            component: ScoreboardComponent,
            canActivateChild: [AuthGuard],
            canActivate: [ReconnectGuard]
          }
        ]
      },
      {path: '**', redirectTo: 'login'}
    ])
  ],
  providers: [AuthGuard, AuthService, GameapiService, ReconnectGuard,PlayGuard,InactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
