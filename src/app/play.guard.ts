import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router";
import { Observable } from "rxjs/Observable";
import { GameapiService } from "./gameapi.service";
import { map, catchError } from "rxjs/operators";
import { of } from "rxjs/observable/of";

@Injectable()
export class PlayGuard implements CanActivate {
  constructor(private gameAPI: GameapiService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.gameAPI.getInRoom().pipe(
      map(e => {
        if (e) {
          return true;
        } else {
          this.router.navigate(["/menu"]);
          return false;
        }
      }),
      catchError(err => {
        this.router.navigate(["/menu"]);
        return of(false);
      })
    );
  }
}
