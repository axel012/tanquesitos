import { TestBed, async, inject } from '@angular/core/testing';

import { ReconnectGuard } from './reconnect.guard';

describe('ReconnectGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReconnectGuard]
    });
  });

  it('should ...', inject([ReconnectGuard], (guard: ReconnectGuard) => {
    expect(guard).toBeTruthy();
  }));
});
