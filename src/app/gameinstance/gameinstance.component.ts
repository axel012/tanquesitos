import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GameapiService } from "../gameapi.service";
import { Observable } from "rxjs";
import { CanComponentDeActivate } from "../inactivate.guard";
declare var p5Loader: any;

@Component({
  selector: "app-gameinstance",
  templateUrl: "./gameinstance.component.html",
  styleUrls: ["./gameinstance.component.css"]
})
export class GameinstanceComponent implements OnInit, CanComponentDeActivate {
  msgProblem: Boolean;
  messageError: String;
  inactivateUser = false;
  constructor(private gameAPI: GameapiService, private router: Router) {
    this.msgProblem = false;
    this.gameAPI.getInRoomPlaying().subscribe(
      success => {
        this.inactivateUser = true;
        let p5loader = new p5Loader();
        p5loader.load();
      },
      err => {
        this.msgProblem = true;
        this.messageError = err.error;
        setTimeout(() => {
          this.msgProblem = false;
          this.inactivateUser = false;
          this.router.navigate(["/menu"]);
        }, 3000);
      }
    );
  }
  async isCurrentlyPlaying() {
    return await this.gameAPI.getInRoomPlaying().toPromise();
  }
  beforeunload() {
    console.log("holis");
    if (this.inactivateUser) {
      this.getInactivateResponse().then(
        success => {
          return true;
        },
        err => {
          return false;
        }
      );
    }
    return true;
  }

  async getInactivateResponse() {
    return await this.gameAPI.getInRoomNotPlaying().toPromise();
  }
  ngOnInit() {}
}
