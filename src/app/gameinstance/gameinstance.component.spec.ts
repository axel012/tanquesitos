import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameinstanceComponent } from './gameinstance.component';

describe('GameinstanceComponent', () => {
  let component: GameinstanceComponent;
  let fixture: ComponentFixture<GameinstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameinstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameinstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
