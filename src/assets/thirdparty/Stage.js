class Stage {
  constructor() {
    this.entities = { Bullet: [], Explosion: [], PowerUp: [], Tank: [] };
    this.cnv = {};
    this.InfPaneVisible = false;
    this.mainPlayerWinLoseMsg = "You Lose!";
    this.stopGame = true;
    this.stopGameCause = Stage.StopGameCauses.Loading;
    this.pingRequested = true;
  }

  static get Instance() {
    if (this._instance === undefined) {
      this._instance = new Stage();
    }
    return this._instance;
  }
  static Destroy() {
    this._instance = undefined;
	try{
	remove();
	}catch(e){}
  }
  static get StopGameCauses() {
    return { killGoalReached: "killGoalReached", Loading: "Loading" };
  }

  addEntity(e) {
    let index = this.entities[e.constructorName].length;
    this.entities[e.constructorName][index] = e;
  }

  removeEntity(e) {
    let index = this.entities[e.constructorName].indexOf(e);
    this.entities[e.constructorName].splice(index, 1);
  }

  addMainPlayer(p) {
    this.mainPlayer = p;
  }

  preloadAssets() {
    Assets.initialize();
    let promises = [];
    let addPromise = function(nameAudio, pathAudio) {
      return new Promise(function(resolve, reject) {
        Assets.registerAsset(nameAudio, Assets.loadASound(pathAudio));
        Assets.get(nameAudio).on("load", function() {
          resolve();
        });
        Assets.get(nameAudio).on("loaderror", function(id, error) {
          reject("audio:" + id + " error:" + error);
        });
      });
    };

    promises.push(addPromise("shot", "/assets/sounds/shot.mp3"));
    promises.push(
      addPromise("missilelaunched", "/assets/sounds/missilelaunched.mp3")
    );
    promises.push(addPromise("movetank", "/assets/sounds/movetank.mp3"));
    promises.push(addPromise("explosionsnd", "/assets/sounds/explosion.mp3"));
    promises.push(
      addPromise("tankExplosion", "/assets/sounds/tankexplosion.mp3")
    );
    promises.push(
      addPromise("HeavyMachineGunsnd", "/assets/sounds/heavymachinegun.mp3")
    );
    promises.push(
      addPromise("RocketLaunchersnd", "/assets/sounds/rocketlauncher.mp3")
    );
    promises.push(
      addPromise("SuperGrenadesnd", "/assets/sounds/supergrenade.mp3")
    );
    promises.push(addPromise("Lifesnd", "/assets/sounds/okay.mp3"));
    promises.push(addPromise("Shieldsnd", "/assets/sounds/okay.mp3"));
    promises.push(addPromise("noBullets", "/assets/sounds/nobullets.mp3"));
    promises.push(addPromise("engine", "/assets/sounds/engine.mp3"));
    promises.push(addPromise("win", "/assets/sounds/win.mp3"));
    promises.push(addPromise("lose", "/assets/sounds/lose.mp3"));

    let preloadAllSoundsSync = async function() {
      await Promise.all(promises);
    };
    preloadAllSoundsSync();

    PressStart2P = loadFont("assets/fonts/PressStart2P.ttf");

    Assets.registerAsset("redtank", loadImage("/assets/images/redtank.png"));
    Assets.registerAsset("bluetank", loadImage("/assets/images/bluetank.png"));
    Assets.registerAsset(
      "greentank",
      loadImage("/assets/images/greentank.png")
    );
    Assets.registerAsset("pinktank", loadImage("/assets/images/pinktank.png"));
    Assets.registerAsset(
      "mushroomexplosion",
      loadImage("/assets/images/mushroomexplosion.png")
    );
    Assets.registerAsset(
      "explosionimg",
      loadImage("/assets/images/explosion.png")
    );
    Assets.registerAsset("shootSuperGrenade", function(colorB) {
      stroke(0, 0, 0);
      strokeWeight(6);
      line(0, 0, 6, 0);
      strokeWeight(2);
      line(8, 0, 9, 0);
      stroke(colorB);
      strokeWeight(2);
      line(-3, 0, 1, 0);
      line(-3, 4.5, 1, 3);
      line(-3, -4.5, 1, -3);
    });
    Assets.registerAsset("ammoIcon", loadImage("/assets/images/ammoicon.png"));
    Assets.registerAsset("misiles", loadImage("/assets/images/misiles.png"));
    Assets.registerAsset("shootMachineGun", function(colorB) {
      stroke(colorB);
      strokeWeight(2);
      line(0, 0, 4, 0);
    });
    Assets.registerAsset(
      "HeavyMachineGunimg",
      loadImage("/assets/images/puminigun.png")
    );
    Assets.registerAsset(
      "RocketLauncherimg",
      loadImage("/assets/images/purocket.png")
    );
    Assets.registerAsset(
      "SuperGrenadeimg",
      loadImage("/assets/images/pusupergrenade.png")
    );
    Assets.registerAsset("Lifeimg", loadImage("/assets/images/pulife.png"));
    Assets.registerAsset("Shieldimg", loadImage("/assets/images/pushield.png"));
    Assets.registerAsset("tileset", loadImage("/assets/maps/tileset.png"));
    Assets.registerAsset("bgTiles", loadImage("/assets/maps/bgTiles.png"));
    Assets.registerAsset(
      "blockTiles",
      loadImage("/assets/maps/blockTiles.png")
    );
    Assets.registerAsset(
      "blockCrack1",
      loadImage("/assets/images/blockcrack1.png")
    );
    Assets.registerAsset(
      "blockCrack2",
      loadImage("/assets/images/blockcrack2.png")
    );

    Assets.registerAsset(
      "brickSmoke",
      loadImage("/assets/images/bricksmoke.png")
    );
    Assets.registerAsset(
      "shootSmoke",
      loadImage("/assets/images/shootsmoke.png")
    );
  }

  loadJSONFile(path, callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", path, true);
    xobj.onreadystatechange = function() {
      if (xobj.readyState == 4 && xobj.status == "200") {
        // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
        callback(xobj.responseText);
      }
    };
    xobj.send(null);
  }

  initialize() {
    this.cnv = createCanvas(displayWidth, displayHeight);
    this.cnv.parent("gamecontainer");
    width = windowWidth;
    height = windowHeight;

    SoundManager.Instance.registerSounds(
      "engine",
      Assets.get("engine"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "movetank",
      Assets.get("movetank"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "shot",
      Assets.get("shot"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "missilelaunched",
      Assets.get("missilelaunched"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "explosion",
      Assets.get("explosionsnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "tankExplosion",
      Assets.get("tankExplosion"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "HeavyMachineGun",
      Assets.get("HeavyMachineGunsnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "RocketLauncher",
      Assets.get("RocketLaunchersnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "SuperGrenade",
      Assets.get("SuperGrenadesnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "Life",
      Assets.get("Lifesnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "Shield",
      Assets.get("Shieldsnd"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "noBullets",
      Assets.get("noBullets"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "win",
      Assets.get("win"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.registerSounds(
      "lose",
      Assets.get("lose"),
      SoundManager.TYPESOUND.EFFECT
    );
    SoundManager.Instance.playSound("engine", true);

    this.camera = new Camera();

    if (this.Map !== undefined) {
      this.Map.onResize();
    }
  }

  drawInfPane() {
    fill(0, 0, 0, 180);
    let widthtab;
    if (TankMap.Instance().numCols * Tile.SIZE > width) {
      widthtab = width;
    } else {
      widthtab = TankMap.Instance().numCols * Tile.SIZE;
    }
    widthtab = (widthtab * 0.56) / TankMap.Instance().scl;
    let heighttab = (height * 0.7) / TankMap.Instance().scl;
    stroke("red");
    strokeWeight(2);
    let xPaneOrig =
      -TankMap.Instance().xo -
      TankMap.Instance().transx +
      width / TankMap.Instance().scl / 2 -
      widthtab / 2;
    let yPaneOrig =
      -TankMap.Instance().transy +
      height / TankMap.Instance().scl / 2 -
      heighttab / 2;
    rect(xPaneOrig, yPaneOrig, widthtab, heighttab, 10);
    translate(xPaneOrig, yPaneOrig);
    textAlign(CENTER, CENTER);
    textFont("PressStart2P");
    textStyle(BOLD);
    noStroke();
    let colCount = 6;
    if (this.stopGame) {
      switch (this.stopGameCause) {
        case Stage.StopGameCauses.killGoalReached:
          textSize(25);
          fill("red");
          text(this.mainPlayerWinLoseMsg, widthtab / 2, 20);
          break;
      }
    } else {
      textSize(19);
      fill("white");
      text("Tanquesitos", widthtab / 2, 20);
    }

    textSize(12);
    fill("#e0e0d1");
    text("Name", widthtab / colCount, 50);
    text("Tank", (widthtab * 2) / colCount, 50);
    text("Kills", (widthtab * 3) / colCount, 50);
    text("Deaths", (widthtab * 4) / colCount, 50);
    text("Ping", (widthtab * 5) / colCount, 50);
    let hposition = 50;
    for (let tank of this.entities["Tank"]) {
      hposition += 40;
      fill(tank.colorTank);
      let imgwt = Assets.get(tank.sprite).width / tank.cantimages;
      let imght = Assets.get(tank.sprite).height;
      let imgt = Assets.get(tank.sprite).get(0, 0, imgwt, imght);
      text(tank.name, widthtab / colCount, hposition);
      image(
        imgt,
        (widthtab * 2) / colCount - imgwt / 2,
        hposition - imght / 2,
        imgwt,
        imght
      );
      text(tank.kills, (widthtab * 3) / colCount, hposition);
      text(tank.deaths, (widthtab * 4) / colCount, hposition);
      let pingToShow = "";
      if (tank.ping < 1000) {
        pingToShow = tank.ping + " ms";
      } else if (tank.ping >= 1000 && tank.ping < 10000) {
        pingToShow = tank.ping + " s";
      } else {
        pingToShow = "Inf";
      }
      text(pingToShow, (widthtab * 5) / colCount, hposition);
    }
  }
  render() {
    background(10, 101, 57);
    push();
    if (this.Map !== undefined) {
      this.Map.render(this.camera);
    }
    for (let group in this.entities) {
      if (group === "Bullet") {
        continue;
      }
      for (let entity of this.entities[group]) {
        if (entity === this.mainPlayer) {
          continue;
        }
        entity.render();
      }
    }
    //render mainPlayer on top of other players
    if (this.mainPlayer !== undefined) {
      this.mainPlayer.playSounds();
      this.mainPlayer.render();
    }

    //render bullets on top
    for (let bullet of this.entities["Bullet"]) {
      bullet.render();
    }
    textSize(18);
    text(floor(frameRate()), 60, 60);
    push();
    if (
      this.InfPaneVisible ||
      (this.stopGame &&
        this.stopGameCause === Stage.StopGameCauses.killGoalReached)
    ) {
      this.drawInfPane();
    }
    pop();
    push();
    if (this.Map !== undefined && this.mainPlayer != undefined) {
      let widthtabBottom = Tile.SIZE * 10;
      let heighttabBottom = Tile.SIZE * 1;
      let xBottomPane =
        -TankMap.Instance().xo -
        TankMap.Instance().transx +
        width / TankMap.Instance().scl / 2 -
        widthtabBottom / 2;
      let yBottomPane;
      if (
        height <
        TankMap.Instance().numRows * Tile.SIZE * TankMap.Instance().scl
      ) {
        yBottomPane =
          -TankMap.Instance().transy +
          (height / TankMap.Instance().scl - 0.5 * Tile.SIZE) -
          heighttabBottom / 2;
      } else {
        yBottomPane =
          -TankMap.Instance().transy +
          (TankMap.Instance().numRows - 0.5) * Tile.SIZE -
          heighttabBottom / 2;
      }
      fill(0, 0, 0, 180);
      stroke("#003399");
      strokeWeight(2);
      rect(xBottomPane, yBottomPane, widthtabBottom, heighttabBottom, 4);
      translate(xBottomPane, yBottomPane);
      noStroke();
      textAlign(LEFT, CENTER);
      textFont("PressStart2P");
      textStyle(BOLD);
      textSize(18);
      let lifeToShow = this.mainPlayer.life < 0 ? 0 : this.mainPlayer.life;
      if (this.mainPlayer.shield > 0) {
        lifeToShow += this.mainPlayer.shield;
        fill("#606368");
      } else {
        fill("#009900");
      }
      if (this.mainPlayer.life < this.mainPlayer.originalLife * 0.2) {
        fill("red");
      }
      rect(
        widthtabBottom / 6 - (heighttabBottom * 0.8) / 2,
        heighttabBottom / 2 - (heighttabBottom * 0.3) / 2,
        heighttabBottom * 0.8,
        heighttabBottom * 0.3,
        3
      );
      rect(
        widthtabBottom / 6 - (heighttabBottom * 0.3) / 2,
        heighttabBottom / 2 - (heighttabBottom * 0.8) / 2,
        heighttabBottom * 0.3,
        heighttabBottom * 0.8,
        3
      );
      text(
        lifeToShow,
        widthtabBottom / 6 + heighttabBottom / 2,
        (heighttabBottom * 1.1) / 2
      );

      fill(0, 0, 0, 200);
      stroke("#cc0000");
      ellipse(
        (widthtabBottom * 3) / 6,
        heighttabBottom / 2,
        heighttabBottom * 0.9,
        heighttabBottom * 0.9
      );
      noStroke();
      textAlign(CENTER, CENTER);
      textSize(19);
      fill("#f5f5f0");
      text(
        this.mainPlayer.weapon.letter,
        (widthtabBottom * 3) / 6,
        (heighttabBottom * 1.1) / 2
      );

      textSize(18);
      textAlign(LEFT, CENTER);
      let imgAmmoIcon = Assets.get("ammoIcon");
      image(
        imgAmmoIcon,
        (widthtabBottom * 4) / 6 - (heighttabBottom * 0.8) / 2,
        heighttabBottom * (1 / 2 - 0.4),
        heighttabBottom * 0.8,
        heighttabBottom * 0.8
      );
      fill("#e6b800");
      let ammoToShow = this.mainPlayer.weapon.munition;
      if (ammoToShow === undefined || ammoToShow < 0) {
        ammoToShow = 0;
      }
      text(
        "x" + ammoToShow,
        (widthtabBottom * 4) / 6 + (heighttabBottom * 0.9) / 2,
        (heighttabBottom * 1.1) / 2
      );
    }
    pop();
    pop();
  }

  update() {
    if (!this.stopGame) {
      if (this.pingRequested && this.mainPlayer !== undefined) {
        this.pingRequested = false;
        NetworkManager.Instance.socket.emit("pingRequested", {
          timeClient: Date.now(),
          pingMainPlayer: this.mainPlayer.ping
        });
        setTimeout(() => {
          this.pingRequested = true;
        }, 1000);
      }
      for (let group in this.entities) {
        for (let entity of this.entities[group]) {
          entity.update();
        }
      }
    }
  }
}
