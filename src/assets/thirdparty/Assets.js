class Assets {
    static initialize() {
        this._assets = {};
    }

    static registerAsset(name, data) {
        this._assets[name] = data;
    }

    static get(name) {
        return this._assets[name];
    }

    static loadASound(pathSound) {
        return new Howl({
            src: [pathSound],
            autoplay: false,
            loop: false,
            volume: 1,
        });
    }
}
