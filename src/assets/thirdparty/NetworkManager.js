class NetworkManager {
  constructor() {
    this.socket = io("http://localhost:3000", {
      transports: ["websocket"],
      upgrade: false,
      'force new connection': false  
    });
      console.log("se volvio a crear");
  }

  static get Instance() {
    if (!this._instance) {
      this._instance = new NetworkManager();
    } 
    return this._instance;
  }

  addEventListener(eventName, func) {
    if (func !== undefined) {
      this.socket.on(eventName, func);
    } else {
      this.socket.on(eventName);
    }
  }

  removeListener(eventName) {
    this.socket.off(eventName);
  }
}
