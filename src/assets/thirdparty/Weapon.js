class Weapon {
    constructor(cloneWeapon) {
        Object.assign(this,cloneWeapon);
    }
}
Weapon.Type = {
    SuperGrenade: 'SuperGrenade',
    HeavyMachineGun: 'HeavyMachineGun',
    RocketLauncher: 'RocketLauncher',
};
