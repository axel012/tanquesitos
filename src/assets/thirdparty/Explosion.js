class Explosion {

    constructor(explosionClone) {
        this.getDataFromClone(explosionClone);
    }

    getDataFromClone(explosionClone) {
        Object.assign(this, explosionClone);
    }

    render() {
        push();
        translate(this.x, this.y);
        let imgw;
        let imgh;
        let img;
        try {
            imgw = Assets.get(this.sprite).width / this.cantimages;
            imgh = Assets.get(this.sprite).height;
            img = Assets.get(this.sprite).get(imgw * (this.imgActual - 1), 0, imgw, imgh);
            this.width = this.width > imgw ? imgw : this.width;
            this.height = this.height > imgh ? imgh : this.height;
            image(img, -this.width / 2, -this.height / 2, this.width, this.height);
        } catch (err) {
        }
        pop();
    }

 
    update() {
        if (this.followedEntity !== undefined) {
            this.x = this.followedEntity.x + this.plusx;
            this.y = this.followedEntity.y + this.plusy;
        }
        this.rate += 1;
        if (this.rate > 5) {
            this.rate = 0;
            this.imgActual += 1;
            if (this.imgActual > this.cantimages) {
                this.imgActual=1;
                this.deleteEntity = true;
            }
        }
    }

    followEntity(e, plusx, plusy) {
        this.followedEntity = e;
        this.plusx = plusx;
        this.plusy = plusy;
    }
}
