class TileManager {
    static registerTileImage(id, img) {
        if (this.imagesTiles === undefined) {
            this.imagesTiles = {};
        }
        if (!this.imagesTiles.hasOwnProperty(id)) {
            this.imagesTiles[id] = img;
        }
    }

    static getTileImage(id) {
        return this.imagesTiles[id];
    }
}

class Tile {
    static get SIZE() {
        return 32;
    }

    constructor(idImg, x, y, breakable, resistance) {
        this.idImg = idImg;
        this.posX = x;
        this.posY = y;
        this.breakable = breakable;
        this.resistance = resistance;
        this.originalResistance = resistance;
        this.walkable = () => {
            return this.breakable && this.resistance <= 0;
        };
    }

    render(id) {
        image(TileManager.getTileImage(id), this.posX, this.posY);
    }
}
