function loadGameScripts(gameFiles) {
  if (gameFiles.length === 0) {
    return;
  }

  var script = document.createElement("script");
  script.onload = function() {
    loadGameScripts(gameFiles);
  };
  script.src = "/assets/thirdparty/" + gameFiles.shift();
  document.head.appendChild(script);
}

let cnv;
let camera;
let mapLoaded = false;
let PressStart2P;
let completeUpdate = false;

function p5Loader() {
  try {
    remove();
  } catch (err) {}
  this.load = function() {
    loadGameScripts(["p5.min.js", "p5.dom.min.js"]);
  };
}

function gameLoader() {
  this.load = function() {
    loadGameScripts([
      "howler.core.min.js",
      "socket.io.js",
      "NetworkManager.js",
      "Bullet.js",
      "Tank.js",
      "Tile.js",
      "Camera.js",
      "Assets.js",
      "TankMap.js",
      "Explosion.js",
      "SoundManager.js",
      "Weapon.js",
      "PowerUp.js",
      "Stage.js",
      "InputManager.js"
    ]);
  };
}

function preload() {
  if (Stage._instance != undefined) {
    completeUpdate = true;
  }
  Stage.Instance.preloadAssets();
}

function setup() {
  frameRate(60);
  Stage.Instance.initialize();

  if (NetworkManager.Instance.socket.connected === false) {
    NetworkManager.Instance.socket.destroy();
    NetworkManager.Instance.socket = io("http://localhost:3000", {
      transports: ["websocket"],
      upgrade: false,
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      reconnectionAttempts: Infinity,
      "force new connection": false
    });
  }
  NetworkManager.Instance.socket.removeAllListeners();
  NetworkManager.Instance.addEventListener("current_map_entities", function(
    data
  ) {
    let entities = data.map.entities;
    if (entities !== undefined) {
      Stage.Instance.entities = {
        Bullet: [],
        Explosion: [],
        PowerUp: [],
        Tank: []
      };
      for (let group in entities) {
        for (let e of entities[group]) {
          switch (e.constructorName) {
            case "Tank":
              Stage.Instance.addEntity(new Tank(e));
              break;
            case "Explosion":
              if (e.sound) {
                SoundManager.Instance.stopSound("explosion");
                SoundManager.Instance.playSound("explosion", false);
              }
              Stage.Instance.addEntity(new Explosion(e));
              break;
            case "Bullet":
              Stage.Instance.addEntity(new Bullet(e));
              break;
            case "PowerUp":
              Stage.Instance.addEntity(new PowerUp(e));
              break;
          }
        }
      }
    }

    data.map.entities = Stage.Instance.entities;
    Stage.Instance.Map = TankMap.Instance(data.mapJSON, data.map);
    mapLoaded = true;
    windowResized();
  });

  if (completeUpdate) {
    completeUpdate = false;
    NetworkManager.Instance.socket.emit("request_complete_update", {});
  }
  NetworkManager.Instance.addEventListener("player_leave", function(
    playerIndex
  ) {
    let entity = Stage.Instance.entities["Tank"][playerIndex];
    Stage.Instance.removeEntity(entity);
  });

  NetworkManager.Instance.addEventListener("entity_removed", function(data) {
    let entity = Stage.Instance.entities[data.group][data.index];
    if (entity instanceof PowerUp) {
      SoundManager.Instance.stopSound(entity.type);
      SoundManager.Instance.playSound(entity.type, false);
    }
    Stage.Instance.removeEntity(entity);
  });

  NetworkManager.Instance.addEventListener("entity_added", function(e) {
    if (e.constructorName === "Tank") {
    }
    switch (e.constructorName) {
      case "Tank":
        Stage.Instance.addEntity(new Tank(e));
        break;
      case "Explosion":
        if (e.sound) {
          SoundManager.Instance.stopSound("explosion");
          SoundManager.Instance.playSound("explosion", false);
        }
        let exp = new Explosion(e);
        Stage.Instance.addEntity(exp);
        if (exp.sprite === "shootSmoke") {
          for (let tk of Stage.Instance.entities["Tank"]) {
            if (tk.id === exp.idTank) {
              let plusx = (Math.cos(tk.rotation) * (tk.h + 18)) / 2;
              let plusy = (Math.sin(tk.rotation) * (tk.h + 18)) / 2;
              exp.followEntity(tk, plusx, plusy);
              break;
            }
          }
        }
        break;
      case "Bullet":
        SoundManager.Instance.stopSound(e.soundShot);
        SoundManager.Instance.playSound(e.soundShot, false);
        Stage.Instance.addEntity(new Bullet(e));
        break;
      case "PowerUp":
        Stage.Instance.addEntity(new PowerUp(e));
        break;
    }
  });

  NetworkManager.Instance.addEventListener("entities_map_updated", function(
    data
  ) {
    let entities = data.entities;
    if (entities !== undefined) {
      for (let group in entities) {
        for (let i = 0; i < entities[group].length; ++i) {
          let e = entities[group][i];
          switch (e.constructorName) {
            case "Tank":
              Stage.Instance.entities[group][i].getDataFromClone(e);
              break;
            case "Explosion":
              Stage.Instance.entities[group][i].getDataFromClone(e);
              break;
            case "Bullet":
              Stage.Instance.entities[group][i].getDataFromClone(e);
              break;
            case "PowerUp":
              Stage.Instance.entities[group][i].getDataFromClone(e);
              break;
          }
        }
      }
    }
    Stage.Instance.Map.renderBlockChanges(data.mapBlockChanges);
  });

  NetworkManager.Instance.addEventListener("main_player_join", function(
    playerIndex
  ) {
    let entity = Stage.Instance.entities["Tank"][playerIndex];
    Stage.Instance.addMainPlayer(entity);
  });

  NetworkManager.Instance.addEventListener("pingResponsed", function(data) {
    Stage.Instance.mainPlayer.ping =
      data.timeToServer + (Date.now() - data.timeServer);
  });

  NetworkManager.Instance.addEventListener("stop_game", function(cause) {
    Stage.Instance.stopGame = true;
    Stage.Instance.stopGameCause = cause;
    switch (Stage.Instance.stopGameCause) {
      case Stage.StopGameCauses.killGoalReached:
        Stage.Instance.mainPlayerWinLoseMsg = "You Win!";
        for (let tank of Stage.Instance.entities["Tank"]) {
          if (tank.id === Stage.Instance.mainPlayer.id) {
            continue;
          }
          if (tank.kills > Stage.Instance.mainPlayer.kills) {
            Stage.Instance.mainPlayerWinLoseMsg = "You Lose!";
          }
        }
        if (Stage.Instance.mainPlayerWinLoseMsg === "You Lose!") {
          SoundManager.Instance.playSound("lose", false);
        } else {
          SoundManager.Instance.playSound("win", false);
        }
        break;
    }
  });

  NetworkManager.Instance.addEventListener("start_play", function(data) {
    Stage.Instance.stopGame = data.stop;
    Stage.Instance.stopGameCause = data.cause;
  });

  InputManager.Instance;
}

function draw() {
  Stage.Instance.update();
  Stage.Instance.render();
}

function windowResized() {
  if (!mapLoaded) return;
  width = windowWidth;
  height = windowHeight;
  try {
    Stage.Instance.cnv.position(0, 0);
  } catch (e) {}
  TankMap.Instance().onResize();
}
