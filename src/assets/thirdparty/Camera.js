class Camera {
  constructor() {
    this.xOffset = 0;
    this.yOffset = 0;
  }

  followEntity(e) {
    if (
      TankMap.Instance().numRows * Tile.SIZE * TankMap.Instance().scl > height ||
      TankMap.Instance().numCols * Tile.SIZE * TankMap.Instance().scl > width
    ) {
      if (width > height) {
        this.xOffset = 0;
        this.yOffset = e.y * TankMap.Instance().scl - height / 2;
        if (this.yOffset < 0) this.yOffset = 0;
        else if (this.yOffset > TankMap.Instance().numRows * Tile.SIZE * TankMap.Instance().scl - height) {
          this.yOffset = TankMap.Instance().numRows * Tile.SIZE * TankMap.Instance().scl - height;
        }
      } else {
        this.xOffset = e.x * TankMap.Instance().scl - width / 2;
        this.yOffset = 0;
        if (this.xOffset < 0) this.xOffset = 0;
        else if (this.xOffset > TankMap.Instance().numCols * Tile.SIZE * TankMap.Instance().scl - width) {
          this.xOffset = TankMap.Instance().numCols * Tile.SIZE * TankMap.Instance().scl - width;
        }
      }
    }
  }
}
