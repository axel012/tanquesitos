class PowerUp {

    constructor(powerupClone) {
        this.getDataFromClone(powerupClone);
    }

    getDataFromClone(powerupClone) {
        Object.assign(this, powerupClone);
        this.getPower = function() {
            this.weapon = new Weapon(this.type);
        };
    }

    render() {
        push();
        translate(this.x, this.y);
        image(Assets.get(this.img), 0, 0, this.w, this.h);
        pop();
    }
    update() {}
}
PowerUp.Type = {
    WEAPON: Weapon.Type,
    LIFE: 'Life',
    SHIELD:'Shiel',
};
