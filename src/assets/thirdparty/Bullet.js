class Bullet {

    constructor(bulletClone) {
        this.getDataFromClone(bulletClone);
    }

    getDataFromClone(bulletClone) {
        for (const prop in bulletClone) {
            this[prop] = bulletClone[prop];
        }
    }

    render() {
        push();
        translate(this.x, this.y);
        rotate(this.angle);
        if (this.imageIsFunction) {
            let drawimage = Assets.get(this.sprite);
            drawimage(this.color);
        } else {
            let imgw = Assets.get(this.sprite).width / this.cantimages;
            let imgh = Assets.get(this.sprite).height;
            let img = Assets.get(this.sprite).get(imgw * (this.imgActual - 1), 0, imgw, imgh);
            image(img, 0, -this.h / 2, this.w, this.h);
        }
        pop();
    }

    crack(thecollidable) {
        let res = thecollidable.resistance / thecollidable.originalResistance;
        let crackImage = {};
        if (res < 0.66) {
            if (res < 0.66 && res >= 0.33) {
                crackImage = 'blockCrack1';
            } else if (res < 0.33) {
                crackImage = 'blockCrack2';
            }
        }
    }
    destroyBlock(thecollidable) {
        if (!(thecollidable.breakable && thecollidable.resistance <= 0)) {
         
            let explosion = new Explosion(
                'brickSmoke',
                thecollidable.posX + Tile.SIZE / 2,
                thecollidable.posY + Tile.SIZE / 2,
                Tile.SIZE,
                Tile.SIZE,
                5,
                false,
            );

            this.explosions.push(explosion);
        } else {
            this.crack(thecollidable);
        }
    }

    destroyNearbyBlocks(centralBlock, callback) {
        if (this.damage > this.commonDamageLimit) {
            let e = Tile.SIZE * 1.0;
            let points = [
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY},
                {px: centralBlock.posX, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY},
                {px: centralBlock.posX, py: centralBlock.posY - e * 0.5},
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY + e * 1.1},
                {px: centralBlock.posX - e * 0.5, py: centralBlock.posY - e * 0.5},
                {px: centralBlock.posX + e * 1.1, py: centralBlock.posY - e * 0.5},
            ];
            for (let point of points) {
                if (point.px < 0) {
                    point.px = 0;
                } else if (point.px > TankMap.Instance().numCols * Tile.SIZE) {
                    point.px = TankMap.Instance().numCols * Tile.SIZE - 1;
                }
                if (point.py < 0) {
                    point.py = 0;
                } else if (point.py > TankMap.Instance().numRows * Tile.SIZE) {
                    point.py = TankMap.Instance().numRows * Tile.SIZE - 1;
                }
                let nearcolli = TankMap.Instance().getThereACollidable(point.px, point.py, 1, 1);
                if (nearcolli !== null) {
                    nearcolli.resistance -= this.damage * 0.7;
                    callback.call(this, nearcolli);
                }
            }
        }
    }

    destroyNearbyTanks(x, y) {
        if (this.damage > this.commonDamageLimit) {
            for (let t of TankMap.Instance().entities['Tank']) {
                let distance = Math.sqrt(Math.pow(t.x - x, 2) + Math.pow(t.y - y, 2));
                if (distance < 1.5 * Tile.SIZE) {
                    t.destroy(this.damage * 0.7);
                    if (t.life < 0 && t.id !== this.idTank) {
                        this.tanksDestroyed++;
                    }
                }
            }
        }
    }
    update() {
        this.rate += 1;
        if (this.rate > 5) {
            this.rate = 0;
            this.imgActual += 1;
            if (this.imgActual > this.cantimages) {
                this.imgActual = 1;
            }
        }
        let colli = null;
        let tank = null;
        let distancia = Math.sqrt(Math.pow(this.xMove, 2) + Math.pow(this.yMove, 2));
        for (let i = 0; i < distancia; i += 3) {
            let addExplosion = (xAux, yAux) => {
                this.xMove = i * Math.cos(this.angle);
                this.yMove = i * Math.sin(this.angle);
                this.deleteEntity = true;
            };
            let xEvaluated = this.x + i * Math.cos(this.angle);
            let yEvaluated = this.y + i * Math.sin(this.angle);
            colli = TankMap.Instance().getThereACollidable(xEvaluated, yEvaluated, this.w, this.h);
            if (colli !== null) {
                colli.resistance -= this.damage;
                this.xMove = i * Math.cos(this.angle);
                this.yMove = i * Math.sin(this.angle);
                this.deleteEntity = true;
                break;
            } else {
                tank = TankMap.Instance().getATank(xEvaluated, yEvaluated, this.w, this.h, this.idTank);
                if (tank !== null) {
                    tank.destroy(this.damage * 0.7);
                    this.xMove = i * Math.cos(this.angle);
                    this.yMove = i * Math.sin(this.angle);
                    this.deleteEntity = true;
                    break;
                }
            }
        }
      
        this.x += this.xMove;
        this.y += this.yMove;
  
    }
}
