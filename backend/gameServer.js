class GameServer {

  constructor() {

  }
//crea el servidor de socket.io y le agrega el session middleware para compartir la session de express con la de sockets
  startUp(server, sessionMiddleWare) {
    this._io = require("socket.io")(server);
    this._io.set('transports', ['websocket']);
    this._io.use(function (socket, next) {
      sessionMiddleWare(socket.request, socket.request.res || {}, next);
    });

  }

  get io() {
    return this._io;
  }

  static get Instance() {
    if (this._instance === undefined) {
      this._instance = new GameServer();
    }
    return this._instance;
  }

}

module.exports = GameServer.Instance;
