const express = require('express');
const path = require('path');
const app = express();
const server = require('http').createServer(app);
const mongoose = require('mongoose');
const bodyParser=require('body-parser');
const cors=require('cors');
const session = require("express-session");

const gameServer = require("./gameServer");

//modelos
require('./models/stats.js');
require('./models/player.js');



//middlewares
app.use(cors({credentials: true, origin: 'http://localhost:4200'}));
app.use(bodyParser.urlencoded({ extended: false  }));
app.use(bodyParser.json());

const sessionMiddleware = session({
	secret: "secret string",
	resave:false,
	saveUninitialized: false,
	name: "sid"
});
app.use(sessionMiddleware);

//start game server
gameServer.startUp(server,sessionMiddleware);
app.use(require('./routes'));

//mongoose conection
mongoose.connect('mongodb://t4nksgame:t4nksgame@ds053449.mlab.com:53449/tankgame');

let router=express.Router();
app.use(router);

//not found error middleware
app.use(function(req,res){res.sendStatus(404);});

const port=3000;

server.listen(port, '127.0.0.1', function() {
	  console.log('Tank API running on 3000');

});







