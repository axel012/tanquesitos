  var mongoose = require('mongoose');
var router = require('express').Router();
var Player = mongoose.model('player');
const util = require('../utils/util');

//Register account
router.post('/', (req, res, next) => {
    let user = req.body.username;
    let password = req.body.password;
	let name = req.body.name;
	
    let objplayer = {
      username: user,
      password: util.passwordHash(password),
	  name:name
    };
    let player = new Player(objplayer);
    player.save(function(err) {
      if (err !== null) {
        return res.status(403).send({error:err});
      } else {
        return res.status(200).send({});
      }
    });
  });



module.exports = router;