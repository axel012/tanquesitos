let mongoose = require("mongoose");
let router = require("express").Router();
let Player = mongoose.model("player");

/** Middleware para verificar si el usuario esta logueado*/
const checkLoggedInRequired = (req, res, next) => {
    if (req.session.userId === undefined) {
        res.status(403).send({ error: "Permission denied" });
    } else {
        next();
    }
};

router.get("/wins",checkLoggedInRequired, (req, res, next) => {
Player.aggregate([{$project:{"wins":"$stats.wongames",name:1,_id:0}}]).sort({"wins":-1}).limit(10)
.then(function(players){
	res.status(200).json(players);
});
});

router.get("/deaths",checkLoggedInRequired, (req, res, next) => {
Player.aggregate([{$project:{"deaths":"$stats.deaths",name:1,_id:0}}]).sort({"deaths":-1}).limit(10)
.then(function(players){
	res.status(200).json(players);
});
});

router.get("/loses",checkLoggedInRequired, (req, res, next) => {
Player.aggregate([{$project:{"loses":{$subtract:["$stats.totalgames","$stats.wongames"]},name:1,_id:0}}]).sort({"loses":-1}).limit(10)
.then(function(players){
	res.status(200).json(players);
});
});


router.get("/kills",checkLoggedInRequired, (req, res, next) => {
Player.aggregate([{$project:{"kills":"$stats.kills",name:1,_id:0}}]).sort({"kills":-1}).limit(10)
.then(function(players){
	res.status(200).json(players);
});
});

router.get("/blocks",checkLoggedInRequired, (req, res, next) => {
Player.aggregate([{$project:{"blocks":"$stats.blocksdestroyed",name:1,_id:0}}]).sort({"blocks":-1}).limit(10)
.then(function(players){
	res.status(200).json(players);
});
});

module.exports = router;