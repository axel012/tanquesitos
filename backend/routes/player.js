var mongoose = require('mongoose');
var router = require('express').Router();
var Player = mongoose.model('player');
const util = require('../utils/util');

/**Middleware para verificar si el usuario esta logueado*/
const checkLoggedInRequired = (req,res,next)=>{
	if(req.session.userId === undefined){
		res.status(403).send({error:"Permission denied"});
	}else{
    next();
	}
}

/**Middleware para verificar si el usuario tiene permisos de administrador*/
const checkHasRights = (req,res,next)=>{
  if(req.session.hasRights){
    next();
  }else{
    res.status(403).send({error:"Permission denied"});
  }
}

//get players
router.get('/',checkLoggedInRequired, (req, res, next) => {
  Player.find({})
    .then(function(players) {
      if (!players) {
        return res.sendStatus(404);
      }
      return res.status(200).json({players: players});
    })
    .catch(function(reject) {
      return res.status(404).send(reject.message);
    });
});


//get player by id
router.get('/:id',checkLoggedInRequired, (req, res, next) => {
  let id = req.params.id;
  Player.findById(id)
    .then(function(player) {
      if (!player) {
        return res.sendStatus(404);
      }
      return res.status(200).json({player: player});
    })
    .catch(function(reject) {
      return res.status(404).send(reject.message);
    });
});


//ADMIN RIGHTS
//PUT
router.put('/:id',checkHasRights, (req, res, next) => {
  let id = req.params.id;
  let stats = req.body.stats;
  let password = req.body.password;
  let objplayer = {};
  if(password !== undefined)  objplayer.password = util.passwordHash(password);
  if(stats !== undefined) objplayer.stats = stats;

  Player.findByIdAndUpdate(id, objplayer, {upsert: false}, function(err, doc) {
    if (err) {
      return res.status(400).send({error: err});
    }
    return res.status(200).send('successful update');
  });
});

//DELETE 
router.delete('/:id',checkHasRights, (req, res, next) => {
  let id = req.params.id;
  Player.findByIdAndRemove(id, function(err, doc) {
    if (err) {
      console.log('delete error:' + err);
      return res.status(400).send({error: err});
    }
    return res.status(200).send('successful delete');
  });
});


module.exports = router;
