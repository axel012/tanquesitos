var mongoose = require("mongoose");
var router = require("express").Router();
const gameServer = require("../gameServer");
const roomManager = require("./../controllers/roomManager");

router.get("/", (req, res) => {
  try {
    roomManager.removePlayerFromRoom(req.session.userId);
    if (req.session.socket_id !== undefined) {
      let socket = gameServer.io.sockets.connected[req.session.socket_id];
      if (socket) {
        socket.disconnect();
      }
    }
    req.session.destroy(err => {
      if (err) {
        res.status(500).send({ error: "Couldn't log out" });
      } else {
        res.status(200).send({});
      }
    });
  } catch (e) {
    res.status(500).send(new Error("Couldn't log out"));
  }
});

module.exports = router;
