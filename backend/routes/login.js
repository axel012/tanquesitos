var mongoose = require("mongoose");
var router = require("express").Router();
var Player = mongoose.model("player");
const util = require("../utils/util");

/** Middleware para verificar si el usuario NO esta logueado*/
const checkNotLoggedIn = (req, res, next) => {
  if (req.session.userId === undefined) {
    next();
  } else {
    res.status(403).send({ error: "Already logged in" });
  }
};

router.post("/", checkNotLoggedIn, (req, res, next) => {
  let user = req.body.username;
  let password = req.body.password;

  let logindata = {
    username: user,
    password: util.passwordHash(password)
  };

  Player.findOne(logindata)
    .then(
      function(data) {
        if (!data ) {
          return res.sendStatus(404);
        }
        var userdata = { id: data._id, name: data.name, stats: data.stats };
        let { _id, hasRights } = data;
        req.session.userId = _id;
        req.session.hasRights = hasRights;
        req.session.userdata = userdata;
          return res.status(200).json(userdata);
      },
      function(err) {
        return res.status(500).send(err.message);
      }
    )
    .catch(rejected => {
      return res.status(403).send(rejected);
    });
});

/** Devuelve el estado del usuario loggedIn: true - false*/
router.get("/", (req, res, next) => {
  let isLoggedIn = req.session.userId ? true : false;
  res.status(200).send({ loggedIn: isLoggedIn });
});

module.exports = router;
