let mongoose = require("mongoose");
let router = require("express").Router();
let Player = mongoose.model("player");
const roomManager = require("./../controllers/roomManager");

/** Middleware para verificar si el usuario esta logueado*/
const checkLoggedInRequired = (req, res, next) => {
    if (req.session.userId === undefined) {
        res.status(403).send("Permission denied");
    } else {
        next();
    }
};

/** Middleware para verificar si el usuario se encuentra dentro de una sala*/
const checkNotInRoom = (req, res, next) => {
    let room = roomManager.findRoomByPlayerId(req.session.userId);
    if (room !== null) {
        res.status(403).send(`Already in room ${room.roomID}`);
    } else {
        next();
    }
};

/** Middleware para verificar si existe una sala*/
const checkRoomExists = (req, res, next) => {
    const roomID = req.params.id;
    let room = roomManager.findRoomById(roomID);
    if (room === undefined) {
        res.status(403).send("room not found");
    } else {
        next();
    }
};

//devuelve una lista de salas no llenas
router.get("/room", checkLoggedInRequired, (req, res, next) => {
    return res.status(200).json({ rooms: roomManager.listRooms() });
});

//devuelve una lista de salas no llenas con paginacion
router.get(
    "/room/create/:start/:end",
    checkLoggedInRequired,
    (req, res, next) => {
        let start = Number(req.params.start || 0);
        let end = Number(req.params.end || 20);

        return res
            .status(200)
            .json({ rooms: roomManager.getRooms(start, req.params.end) });
    }
);

//creador de salas tipo dummy (falsas) para testeo
router.get("/room/dummy", (req, res, next) => {
    var data = {
        id: Math.floor(Math.random() * 10000 + 1),
        name: `dummy${Math.floor(Math.random() * 10000)}`
    };
    data.userdata = { stats: {} };
    var roomdata = roomManager.addRoom(data);
    for (var i = 0; i < Math.floor(Math.random() * 4); i++) {
        var data = {
            id: Math.floor(Math.random() * 10000 + 1),
            name: `dummy${Math.floor(Math.random() * 10000)}`
        };
        data.userdata = { stats: {} };
        roomManager.join(roomdata.roomID, data);
    }
    const { roomID, _players } = roomManager.findRoomById(roomdata.roomID);
    return res.status(200).json({ id: roomID, players: _players });
});

router.get("/room/create",[checkLoggedInRequired, checkNotInRoom],(req, res, next) => {
        Player.findById(req.session.userId)
            .then(function(player) {
                if (!player) {
                    return res.sendStatus(404);
                }
                var data = { id: player.id, name: player.name };
                data.userdata = { stats: player.stats, username: player.username };
                var roomdata = roomManager.addRoom(data);
                req.session.inroom = roomdata.roomID;
                return res.status(200).json(roomdata);
            })
            .catch(function(reject) {
                return res.status(404).send(reject.message);
            });
    }
);

/** Determina si un usuario se encuentra en una sala activa*/

router.get("/room/active", checkLoggedInRequired, (req, res, next) => {
    let room = roomManager.findRoomByPlayerId(req.session.userId);
    if (!req.session.inroom || req.session.inroom === -1) {
        req.session.inroom = -1;
        return res.status(404).send("player without room yet");
    } else {
        return res
            .status(200)
            .send({ id: req.session.userId, room: req.session.inroom });
    }
});

/** Determina si un usuario puede arrancar a jugar o no xq ya lo esta haciendo*/

router.get("/room/active/playing", checkLoggedInRequired, (req, res, next) => {
    if (req.session.alreadyPlaying === undefined) {
        req.session.alreadyPlaying = true;
        return res.status(200).json({ msg:"Ok" });
        
    } else {
        return res
            .status(403)
            .send("User is already playing in another game instance");
    }
});


/*establece que un usuario no esta jugando pero si en la sala */
router.get("/room/active/disconnect", checkLoggedInRequired, (req, res, next) => {
    if(req.session.alreadyPlaying===true){
        req.session.alreadyPlaying=undefined;
    }
        return res.status(200).json({ msg:"Ok" });
});

router.get(
    "/room/join/:id",
    [checkLoggedInRequired, checkRoomExists, checkNotInRoom],
    (req, res, next) => {
        const roomID = req.params.id;
        Player.findById(req.session.userId)
            .then(function(player) {
                if (!player) {
                    return res.sendStatus(404);
                }
                var data = { id: player.id, name: player.name };
                data.userdata = { stats: player.stats, username: player.username };
                var roomdata = roomManager.join(roomID, data);
                if (roomdata.roomID) req.session.inroom = roomdata.roomID;
                return res.status(200).json(roomdata);
            })
            .catch(function(reject) {
                return res.status(404).send(reject.message);
            });
    }
);

module.exports = router;
