const gameServer = require("../gameServer");
const Room = require("../tankgame/room");
/**
Singleton que posee las salas de juego y los metodos para actualizarlas/llenarlas
*/
class RoomManager {
	
  constructor() {
    this._rooms = {};
    this._lastRoomID = 0;

    gameServer.io.on("connection", socket => {
      let roomID = socket.request.session.inroom;
      if (roomID !== undefined) {
        let room = this.findRoomById(roomID);
        if (room !== undefined) {
          room.onConnection(socket);
        }
      }
    });

    let updateInterval = setInterval(() => {
      this.updateRooms();
    }, 17);

    let updateClientInterval = setInterval(() => {
      this.clientUpdate();
    }, 100);
  }

  static get Instance() {
    if (RoomManager._instance === undefined) {
      RoomManager._instance = new RoomManager();
    }
    return RoomManager._instance;
  }

  updateRooms() {
    try {
      let roomID = Object.keys(this._rooms);
      for (var i = 0; i < roomID.length; i++) {
        this._rooms[roomID[i]].update();
      }
    } catch (err) {
      throw err;
    }
  }

  clientUpdate() {
    try {
      let roomID = Object.keys(this._rooms);
      for (var i = 0; i < roomID.length; i++) {
        this._rooms[roomID[i]].clientUpdate();
      }
    } catch (err) {
      throw err;
    }
  }

  findRoomById(roomID) {
    return this._rooms[roomID];
  }

  findRoomByPlayerId(playerID) {
    try {
      let roomID = Object.keys(this._rooms);
      for (var i = 0; i < roomID.length; i++) {
        if (this._rooms[roomID[i]].hasPlayer(playerID)) {
          return this._rooms[roomID[i]];
        }
      }
      return null;
    } catch (err) {
      throw err;
    }
  }

  //devuelve una lista de salas no llenas
  listRooms() {
    try {
      let rooms = [];
      for (let k of Object.keys(this._rooms)) {
        let room = this._rooms[k];
        if (room._isFull()) continue;
        rooms.push({ players: room.playerCount(), id: room.roomID });
      }
      return rooms;
    } catch (err) {
      throw err;
    }
  }
  
  //devuelve una lista de salas con paginacion
  getRooms(start, count) {
    try {
      let rooms = [];
      let skip = start;
      let keys = Object.keys(this._rooms);
      for (let k of keys) {
        let room = this._rooms[k];
        if (room._isFull()) continue;
        if (skip > 0) {
          skip--;
          continue;
        }
        rooms.push({ players: room.playerCount(), id: room.roomID });
        if (rooms.length >= count) break;
      }
      return rooms;
    } catch (err) {
      throw err;
    }
  }

  //Create room
  addRoom(playerData) {
    try {
      this._lastRoomID++;
      let room = new Room(this._lastRoomID, gameServer.io);
      this._rooms[this._lastRoomID] = room;
      return room.join(playerData);
    } catch (err) {
      throw err;
    }
  }

  //Room join request
  join(roomID, playerData) {
    try {
      var room = this._rooms[roomID];
      if (room) {
        return room.join(playerData);
      } else {
        throw new Error("room not found");
      }
    } catch (err) {
      throw err;
    }
  }

  removePlayerFromRoom(playerID) {
    try {
      let room = this.findRoomByPlayerId(playerID);
      if (room === null) {
        return { error: "player without room asociated" };
      } else {
        room.removePlayer(playerID);
        if (room._isEmpty()) {
          delete this._rooms[room.roomID];
        }
        //aca deberia subir las stats
      }
    } catch (err) {
      throw err;
    }
  }
}

module.exports = RoomManager.Instance;
