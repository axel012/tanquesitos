const sha1 = require('sha1');
const salt = sha1("this is a salt");
const crypto = require('crypto');

function passwordHash(password){
    return sha1(salt + password);
}
function getUniqueNewId(){
    return crypto.randomBytes(20).toString('hex');
}

module.exports = {passwordHash,getUniqueNewId};
