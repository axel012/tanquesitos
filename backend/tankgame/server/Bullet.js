const Explosion = require("./Explosion.js");
const Tile = require("./Tile.js").Tile;
class Bullet {
  constructor(
    sprite,
    x,
    y,
    w,
    h,
    speed,
    angle,
    damage,
    cantimages,
    soundShot,
    imageIsFunction,
    idTank,
    colorTank
  ) {
    this.sprite = sprite;
    this.imageIsFunction = imageIsFunction;
    if (!imageIsFunction) {
      this.imgActual = 1;
      this.cantimages = cantimages;
      this.rate = 0;
    }
    this.x = x;
    this.y = y;
    this.speed = speed;
    this.w = w;
    this.h = h;
    this.angle = angle;
    this.damage = damage;
    this.xMove = speed * Math.cos(angle);
    this.yMove = speed * Math.sin(angle);
    this.deleteEntity = false;
    this.explosions = [];
    this.soundShot = soundShot;
    this.idTank = idTank;
    this.tanksDestroyed = 0;
    this.commonDamageLimit = 300;
    this.color = colorTank;
  }

  render() {
    push();
    translate(this.x, this.y);
    rotate(this.angle);
    if (typeof this.sprite === "function") {
      this.sprite();
    } else {
      let imgw = Assets.get(this.sprite).width / this.cantimages;
      let imgh = Assets.get(this.sprite).height;
      let img = Assets.get(this.sprite).get(
        imgw * (this.imgActual - 1),
        0,
        imgw,
        imgh
      );
      image(img, 0, -this.h / 2, this.w, this.h);
    }
    pop();
  }

  crack(thecollidable, map) {
    let res = thecollidable.resistance / thecollidable.originalResistance;
    let crackImage = {};
    if (res < 0.66) {
      if (res < 0.66 && res >= 0.33) {
        crackImage = "blockCrack1";
      } else if (res < 0.33) {
        crackImage = "blockCrack2";
      }
      map.blockChanges.push({
        type: "crack",
        row: Math.trunc(thecollidable.posY / Tile.SIZE),
        col: Math.trunc(thecollidable.posX / Tile.SIZE),
        resistance: thecollidable.resistance,
        image: crackImage
      });
    }
  }
  destroyBlock(thecollidable, map) {
    if (thecollidable.walkable()) {
      map.blockChanges.push({
        type: "deleteEntity",
        row: Math.trunc(thecollidable.posY / Tile.SIZE),
        col: Math.trunc(thecollidable.posX / Tile.SIZE)
      });
      thecollidable.idImg = -1;
      let explosion = new Explosion(
        "brickSmoke",
        thecollidable.posX + Tile.SIZE / 2,
        thecollidable.posY + Tile.SIZE / 2,
        Tile.SIZE,
        Tile.SIZE,
        5,
        false,
        this.idTank
      );
      this.explosions.push(explosion);
    } else {
      this.crack(thecollidable, map);
    }
  }

  destroyNearbyBlocks(centralBlock, callback, map) {
    if (this.damage > this.commonDamageLimit) {
      let e = Tile.SIZE * 1.0;
      //let d = Math.cos(Math.PI / 4) * e;
      let points = [
        { px: centralBlock.posX + e * 1.1, py: centralBlock.posY },
        { px: centralBlock.posX, py: centralBlock.posY + e * 1.1 },
        { px: centralBlock.posX - e * 0.5, py: centralBlock.posY },
        { px: centralBlock.posX, py: centralBlock.posY - e * 0.5 },
        { px: centralBlock.posX + e * 1.1, py: centralBlock.posY + e * 1.1 },
        { px: centralBlock.posX - e * 0.5, py: centralBlock.posY + e * 1.1 },
        { px: centralBlock.posX - e * 0.5, py: centralBlock.posY - e * 0.5 },
        { px: centralBlock.posX + e * 1.1, py: centralBlock.posY - e * 0.5 }
      ];
      for (let point of points) {
        if (point.px < 0) {
          point.px = 0;
        } else if (point.px > map.numCols * Tile.SIZE) {
          point.px = map.numCols * Tile.SIZE - 1;
        }
        if (point.py < 0) {
          point.py = 0;
        } else if (point.py > map.numRows * Tile.SIZE) {
          point.py = map.numRows * Tile.SIZE - 1;
        }
        let nearcolli = map.getThereACollidable(point.px, point.py, 1, 1);
        if (nearcolli !== null) {
          nearcolli.resistance -= this.damage * 0.7;
          callback.call(this, nearcolli, map);
        }
      }
    }
  }

  destroyNearbyTanks(x, y, map) {
    if (this.damage > this.commonDamageLimit) {
      for (let t of map.entities["Tank"]) {
        let distance = Math.sqrt(Math.pow(t.x - x, 2) + Math.pow(t.y - y, 2));
        if (distance < 1.5 * Tile.SIZE) {
          t.destroy(this.damage * 0.7);
          if (t.life < 0 && t.id !== this.idTank) {
            this.tanksDestroyed++;
          }
          if (t.id === this.idTank) {
            t.kills--;
            if (t.kills < 0) {
              t.kills = 0;
            }
          }
        }
      }
    }
  }

  update(map) {
    this.rate += 1;
    if (this.rate > 5) {
      this.rate = 0;
      this.imgActual += 1;
      if (this.imgActual > this.cantimages) {
        this.imgActual = 1;
      }
    }
    let colli = null;
    let tank = null;
    let distancia = Math.sqrt(
      Math.pow(this.xMove, 2) + Math.pow(this.yMove, 2)
    );
    for (let i = 0; i < distancia; i += 3) {
      let addExplosion = (xAux, yAux) => {
        this.xMove = i * Math.cos(this.angle);
        this.yMove = i * Math.sin(this.angle);
        this.deleteEntity = true;
        let explX = this.damage > 300 ? xAux : this.x + this.xMove;
        let explY = this.damage > 300 ? yAux : this.y + this.yMove;
        let explosion = new Explosion(
          "explosionimg",
          explX,
          explY,
          this.damage,
          this.damage,
          6,
          true,
          this.idTank
        );
        this.explosions.push(explosion);
      };
      let xEvaluated = this.x + i * Math.cos(this.angle);
      let yEvaluated = this.y + i * Math.sin(this.angle);
      colli = map.getThereACollidable(xEvaluated, yEvaluated, this.w, this.h);
      if (colli !== null) {
        colli.resistance -= this.damage;
        this.destroyNearbyTanks(
          colli.posX + Tile.SIZE / 2,
          colli.posY + Tile.SIZE / 2,
          map
        );
        this.destroyNearbyBlocks(colli, this.destroyBlock, map);
        this.destroyBlock(colli, map);
        addExplosion(colli.posX + Tile.SIZE / 2, colli.posY + Tile.SIZE / 2);
        break;
      } else {
        tank = map.getATank(
          xEvaluated,
          yEvaluated,
          this.w,
          this.h,
          this.idTank
        );
        if (tank !== null) {
          if (tank.id === this.idTank) {
            tank.kills--;
            if (tank.kills < 0) {
              tank.kills = 0;
            }
          }
          this.destroyNearbyTanks(tank.x, tank.y, map);
          let blockOfTank = {
            posX: Math.trunc(tank.x / Tile.SIZE) * Tile.SIZE,
            posY: Math.trunc(tank.y / Tile.SIZE) * Tile.SIZE
          };
          this.destroyNearbyBlocks(blockOfTank, this.destroyBlock, map);
          tank.destroy(this.damage * 0.7);
          if (tank.life < 0) {
            this.tanksDestroyed++;
          }
          addExplosion(tank.x, tank.y);
          break;
        }
      }
    }

    this.x += this.xMove;
    this.y += this.yMove;
  }
}
module.exports = Bullet;
