const Assets = require("./Assets.js");
const Bullet = require("./Bullet.js");
const Map = require("./Map.js");
const PowerUp = require("./PowerUp.js");
const Tile = require("./Tile.js").Tile;
const Explosion = require("./Explosion.js");
const fs = require("fs");

class Stage {
  constructor(io, roomID, killGoal) {
    this.io = io;
    this.roomID = roomID;
    this.killGoal = killGoal | 3;
    this.stopGame = false;
    this.stopGameCause = Stage.StopGameCauses.killGoalReached;
    this.entities = { Bullet: [], Explosion: [], PowerUp: [], Tank: [] };
  }

  static get StopGameCauses() {
    return { killGoalReached: "killGoalReached",
             Loading:"Loading"};
  }

  addEntity(e) {
    e.constructorName = e.constructor.name;
    let index = this.entities[e.constructorName].length;
    this.entities[e.constructorName][index] = e;
    this.io.to(this.roomID).emit("entity_added", e);
  }

  removeEntity(e) {
    let index = this.entities[e.constructorName].indexOf(e);
    this.entities[e.constructorName].splice(index, 1);
  }

  addMainPlayer(p) {
    this.mainPlayer = p;
  }

  initialize() {
    Assets.initialize();
    Assets.registerAsset(
      "map",
      JSON.parse(
        fs.readFileSync("./backend/tankgame/server/maps/mapa1.json", "utf8")
      )
    );
    this.currentMapJSON = Assets.get("map");
    this.Map = new Map(Assets.get("map"), this.entities);
    this.addEntity(
      new PowerUp(
        PowerUp.Type.WEAPON.HeavyMachineGun,
        12 * Tile.SIZE,
        12 * Tile.SIZE,
        Tile.SIZE,
        Tile.SIZE
      )
    );

    //create powerups
    setInterval(() => {
      if (this.entities["PowerUp"].length < 5) {
        let xAppear =
          Math.floor(Math.random() * (this.Map.numCols - 1)) * Tile.SIZE;
        let yAppear =
          Math.floor(Math.random() * (this.Map.numRows - 1)) * Tile.SIZE;
        if (
          this.Map.getThereACollidable(
            xAppear,
            yAppear,
            Tile.SIZE,
            Tile.SIZE
          ) === null
        ) {
          let powerupsNames = [];
          for (let pu in PowerUp.Type) {
            if (typeof PowerUp.Type[pu] === "object") {
              powerupsNames = powerupsNames.concat(
                Object.keys(PowerUp.Type[pu])
              );
            } else {
              powerupsNames[powerupsNames.length] = PowerUp.Type[pu];
            }
          }

          let powerupRandom =
            powerupsNames[Math.floor(powerupsNames.length * Math.random())];
          if (
            this.Map.getAPowerUp(xAppear, yAppear, Tile.SIZE, Tile.SIZE) ===
            null
          ) {
            this.addEntity(
              new PowerUp(powerupRandom, xAppear, yAppear, Tile.SIZE, Tile.SIZE)
            );
          }
        }
      }
    }, 2000);
  }

  update(dt) {
    if (!this.stopGame) {
      for (let group in this.entities) {
        for (let entity of this.entities[group]) {
          entity.update(this.Map);
        }
      }
      for (let i = this.entities["Bullet"].length - 1; i >= 0; --i) {
        let b = this.entities["Bullet"][i];
        let condition =
          b.x < 0 ||
          b.y < 0 ||
          b.x > this.Map.numCols * Tile.SIZE ||
          b.y > this.Map.numRows * Tile.SIZE;
        if (condition) {
          this.entities["Bullet"].splice(i, 1);
          this.io
            .to(this.roomID)
            .emit("entity_removed", { group: "Bullet", index: i });
        }
      }
      for (let tank of this.entities["Tank"]) {
        if (tank.destroyed) {
          tank.streaKills = 0;
          if (tank.reappear) {
            tank.deaths++;
            tank.reappear = false;
            tank.destroyed = false;
            let reappearPoints = [
              { px: 3 * Tile.SIZE, py: 3 * Tile.SIZE },
              { px: 21 * Tile.SIZE, py: 3 * Tile.SIZE },
              { px: 3 * Tile.SIZE, py: 21 * Tile.SIZE },
              { px: 21 * Tile.SIZE, py: 21 * Tile.SIZE }
            ];
            let poinDistances = [];
            let pointToAppear = {};
            let maxDistance = 0;
            for (let point of reappearPoints) {
              let distance = 0;
              for (let t of this.entities["Tank"]) {
                if (t.destroyed) {
                  continue;
                }
                distance += Math.sqrt(
                  Math.pow(point.px - t.x, 2) + Math.pow(point.py - t.y, 2)
                );
              }
              if (distance > maxDistance) {
                maxDistance = distance;
                pointToAppear = point;
              }
            }
            let pxAppear = pointToAppear.px + tank.w / 2;
            let pyAppear = pointToAppear.py + tank.h / 2;
            if (
              this.Map.getThereACollidable(
                pxAppear,
                pyAppear,
                tank.w,
                tank.h
              ) === null &&
              this.Map.getATank(pxAppear, pyAppear, tank.w, tank.h, tank.id) ===
                null
            ) {
              tank.x = pxAppear;
              tank.y = pyAppear;
            }
          }
        }
        for (let i = 0; i < tank.bullets.length; ++i) {
          this.addEntity(tank.bullets[i]);
          let plusx = (Math.cos(tank.rotation) * (tank.h + 18)) / 2;
          let plusy = (Math.sin(tank.rotation) * (tank.h + 18)) / 2;
          let xe = tank.x + plusx;
          let ye = tank.y + plusy;
          let explosion = new Explosion(
            "shootSmoke",
            xe,
            ye,
            Tile.SIZE / 1.5,
            Tile.SIZE / 1.5,
            6,
            false,
            tank.id
          );
          explosion.followEntity(tank, plusx, plusy);
          this.addEntity(explosion);
        }
        tank.bullets = [];
      }
      for (let bullet of this.entities["Bullet"]) {
        if (bullet.tanksDestroyed > 0) {
          for (let tank of this.entities["Tank"]) {
            if (tank.id === bullet.idTank) {
              tank.streaKills += bullet.tanksDestroyed;
              tank.kills += bullet.tanksDestroyed;
              if (tank.kills >= this.killGoal) {
                this.stopGame = true;
                this.stopGameCause=Stage.StopGameCauses.killGoalReached;  
                this.io
                  .to(this.roomID)
                  .emit("stop_game", this.stopGameCause );
              }
              break;
            }
          }
        }
        for (let i = 0; i < bullet.explosions.length; ++i) {
          this.addEntity(bullet.explosions[i]);
        }
        bullet.explosions = [];
      }
      for (let group in this.entities) {
        for (let i = this.entities[group].length - 1; i >= 0; --i) {
          if (this.entities[group][i].deleteEntity === true) {
            this.entities[group].splice(i, 1);
            this.io
              .to(this.roomID)
              .emit("entity_removed", { group: group, index: i });
          }
        }
      }
    }
  }
}
module.exports = Stage;
