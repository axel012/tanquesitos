const Stage = require("./server/Stage.js");
const Tank = require("./server/Tank.js");
const Weapon = require("./server/Weapon.js");
const Tile = require("./server/Tile.js").Tile;

class Room {
  constructor(roomID, io) {
    this.roomID = roomID;
    this._roomID = `room${this.roomID}`;
    this.stage = new Stage(io, this._roomID);
    this.stage.initialize();
    this._players = {};
    // this.sockets = [];
  }

  hasPlayer(playerID) {
    return Object.keys(this._players).indexOf(playerID) >= 0;
  }

  playerCount() {
    return Object.keys(this._players).length;
  }

  _isFull() {
    return Object.keys(this._players).length >= 4;
  }
  _isEmpty() {
    return Object.keys(this._players).length <= 0;
  }

  removePlayer(playerID) {
    if (this.hasPlayer(playerID)) {
      delete this._players[playerID];
    } else {
      throw new Error("player not exists in room");
    }
  }

  join(player) {
    if (this._players[player.id]) {
      throw new Error("already in room");
    }
    if (this._isFull()) {
      throw new Error("room full");
    }

    let roomIndex = player.inroom || -1;
    if (roomIndex != -1 && roomIndex != this.roomID) {
      throw new Error(`already in room nº ${roomIndex}`);
    }

    this._players[player.id] = player.userdata;
    return { data: player.userdata, roomID: this.roomID };
  }

  onConnection(socket) {
    //let so = this.sockets.find(sck => {
    //  return sck.request.session.userId === socket.request.session.userId;
    //});
    //if (!so) {
    //  this.sockets.push(socket);
    //} else {
    //  socket = so;
    //}
    socket.join(this._roomID, () => {
      let SI = this.stage;
      let tankName = socket.request.session.userdata.name;
      socket.request.session.socket_id = socket.id;
      socket.request.session.save();

      let p;
      socket.emit("current_map_entities", {
        map: SI.Map,
        mapJSON: SI.currentMapJSON
      });
      socket.emit("entro_if", { msg: "adentroif" });
      if (SI.entities["Tank"].length === 0) {
        p = new Tank(
          "bluetank",
          3 * Tile.SIZE,
          3 * Tile.SIZE,
          7,
          new Weapon(Weapon.Type.SuperGrenade),
          "blue",
          tankName
        );
      } else if (SI.entities["Tank"].length === 1) {
        p = new Tank(
          "redtank",
          21 * Tile.SIZE,
          3 * Tile.SIZE,
          7,
          new Weapon(Weapon.Type.SuperGrenade),
          "red",
          tankName
        );
      } else if (SI.entities["Tank"].length === 2) {
        p = new Tank(
          "greentank",
          3 * Tile.SIZE,
          21 * Tile.SIZE,
          7,
          new Weapon(Weapon.Type.SuperGrenade),
          "#006600",
          tankName
        );
      } else if (SI.entities["Tank"].length === 3) {
        p = new Tank(
          "pinktank",
          21 * Tile.SIZE,
          21 * Tile.SIZE,
          7,
          new Weapon(Weapon.Type.SuperGrenade),
          "#cc3399",
          tankName
        );
      }
      let playerID = this.stage.entities["Tank"].length;

      p.constructorName = p.constructor.name;
      socket.playerID = playerID;
      this.stage.addEntity(p);
      socket.emit("main_player_join", SI.entities["Tank"].indexOf(p));
      socket.request.session.mainPlayerAdded = true;
      socket.on("request_complete_update", data => {
        socket.emit("current_map_entities", {
          map: SI.Map,
          mapJSON: SI.currentMapJSON
        });
        socket.emit("start_play", {
          stop: SI.stopGame,
          cause: SI.stopGameCause
        });
        socket.emit("main_player_join", SI.entities["Tank"].indexOf(p));
      });
      socket.emit("start_play", { stop: SI.stopGame, cause: SI.stopGameCause });

      socket.on("key_press", mapKeys => {
        if (this.stage.entities["Tank"][socket.playerID] === undefined) return;
        this.stage.entities["Tank"][socket.playerID].mapKeys = mapKeys;
      });
      socket.on("key_release", mapKeys => {
        if (this.stage.entities["Tank"][socket.playerID] === undefined) return;
        this.stage.entities["Tank"][socket.playerID].handleKeysReleased(
          mapKeys
        );
      });
      socket.on("pingRequested", data => {
        if (this.stage.entities["Tank"][socket.playerID] === undefined) return;
        this.stage.entities["Tank"][socket.playerID].ping = data.pingMainPlayer;
        let timeToServer = Date.now() - data.timeClient;
        socket.emit("pingResponsed", {
          timeToServer: timeToServer,
          timeServer: Date.now()
        });
      });
      socket.on("disconnect", () => {
        socket.request.session.alreadyPlaying = undefined;
        this.stage.io
          .to(this._roomID)
          .emit("player_leave", SI.entities["Tank"].indexOf(p));
        this.stage.removeEntity(p);
        p.disconnected = true;
        let connected = this.stage.io.clients().connected;
        for (let k of Object.keys(connected)) {
          let sock = connected[k].rooms[this._roomID];
          if (sock !== undefined) {
            if (connected[k].playerID > socket.playerID) {
              connected[k].playerID--;
            }
          }
        }
      });
    });
  }

  update() {
    this.stage.update();
  }

  clientUpdate() {
    let entitiesToSend = Object.assign({}, this.stage.entities);
    delete entitiesToSend["PowerUp"];
    delete entitiesToSend["Explosion"];
    this.stage.io.to(this._roomID).emit("entities_map_updated", {
      entities: entitiesToSend,
      mapBlockChanges: this.stage.Map.blockChanges
    });
    this.stage.Map.blockChanges = [];
  }
}

module.exports = Room;
